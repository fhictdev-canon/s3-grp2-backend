FROM gradle:7.5.0-jdk17

WORKDIR /opt/app

COPY ./ ./

RUN gradle assemble

ENTRYPOINT ["java", "-jar", "build/libs/PrintHead_Health-0.0.1-SNAPSHOT.jar"]