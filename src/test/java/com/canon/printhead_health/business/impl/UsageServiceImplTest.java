package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.domain.GetUsageResponse;
import com.canon.printhead_health.domain.Usage;
import com.canon.printhead_health.persistence.entity.UsageEntity;
import com.canon.printhead_health.persistence.repository.UsageRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UsageServiceImplTest {
//    @Mock
//    private UsageRepository usageRepository;
//
//    @InjectMocks
//    private UsageServiceImpl usageService;
//
//    @Test
//    void getUsageById() {
//        UsageEntity usageEntity1 = UsageEntity.builder()
//                .usageId(1L)
//                .printer(1L)
//                .usageDate(LocalDate.of(2020, 2, 1))
//                .positionInEngineId(1L)
//                .printHeadId(1L)
//                .installTimeStamp(LocalDate.of(2020, 2, 1))
//                .lastUsedTimeStamp(LocalDate.of(2020, 2, 1))
//                .nofSquareMmPrinted(1L)
//                .nofVibrationActions(1L)
//                .nofJetActuations(1L)
//                .nofColdStartups(1L)
//                .inkUsageMl(1L)
//                .warmTimeHrs(1L)
//                .biasVoltageTimeHrs(1L)
//                .daysInUse(1L)
//                .build();
//
//        UsageEntity usageEntity2 = UsageEntity.builder()
//                .usageId(2L)
//                .printer(2L)
//                .usageDate(LocalDate.of(2020, 2, 2))
//                .positionInEngineId(2L)
//                .printHeadId(2L)
//                .installTimeStamp(LocalDate.of(2020, 2, 2))
//                .lastUsedTimeStamp(LocalDate.of(2020, 2, 2))
//                .nofSquareMmPrinted(2L)
//                .nofVibrationActions(2L)
//                .nofJetActuations(2L)
//                .nofColdStartups(2L)
//                .inkUsageMl(2L)
//                .warmTimeHrs(2L)
//                .biasVoltageTimeHrs(2L)
//                .daysInUse(2L)
//                .build();
//
//        when(usageRepository.findById(1L)).thenReturn(java.util.Optional.of(usageEntity1));
//        when(usageRepository.findById(2L)).thenReturn(java.util.Optional.of(usageEntity2));
//
//        GetUsageResponse response1 = usageService.getUsageById(1L);
//        GetUsageResponse response2 = usageService.getUsageById(2L);
//
//        Usage usage1 = Usage.builder()
//                .usageId(1L)
//                .printer(1L)
//                .usageDate(LocalDate.of(2020, 2, 1))
//                .positionInEngineId(1L)
//                .printHeadId(1L)
//                .installTimeStamp(LocalDate.of(2020, 2, 1))
//                .lastUsedTimeStamp(LocalDate.of(2020, 2, 1))
//                .nofSquareMmPrinted(1L)
//                .nofVibrationActions(1L)
//                .nofJetActuations(1L)
//                .nofColdStartups(1L)
//                .inkUsageMl(1L)
//                .warmTimeHrs(1L)
//                .biasVoltageTimeHrs(1L)
//                .daysInUse(1L)
//                .build();
//
//        Usage usage2 = Usage.builder()
//                .usageId(2L)
//                .printer(2L)
//                .usageDate(LocalDate.of(2020, 2, 2))
//                .positionInEngineId(2L)
//                .printHeadId(2L)
//                .installTimeStamp(LocalDate.of(2020, 2, 2))
//                .lastUsedTimeStamp(LocalDate.of(2020, 2, 2))
//                .nofSquareMmPrinted(2L)
//                .nofVibrationActions(2L)
//                .nofJetActuations(2L)
//                .nofColdStartups(2L)
//                .inkUsageMl(2L)
//                .warmTimeHrs(2L)
//                .biasVoltageTimeHrs(2L)
//                .daysInUse(2L)
//                .build();
//
//        GetUsageResponse expectedResponse1 = GetUsageResponse.builder()
//                .printHeadUsage(List.of(usage1))
//                .build();
//
//        GetUsageResponse expectedResponse2 = GetUsageResponse.builder()
//                .printHeadUsage(List.of(usage2))
//                .build();
//
//        assertEquals(expectedResponse1, response1);
//        assertEquals(expectedResponse2, response2);
//    }
//
//    @Test
//    void getUsageByDate() {
//        LocalDate date1 = LocalDate.of(2020, 2, 1);
//        LocalDate date2 = LocalDate.of(2020, 2, 3);
//        UsageEntity usageEntity1 = UsageEntity.builder()
//                .usageId(1L)
//                .printer(1L)
//                .usageDate(LocalDate.of(2020, 2, 2))
//                .positionInEngineId(1L)
//                .printHeadId(1L)
//                .installTimeStamp(LocalDate.of(2020, 2, 1))
//                .lastUsedTimeStamp(LocalDate.of(2020, 2, 1))
//                .nofSquareMmPrinted(1L)
//                .nofVibrationActions(1L)
//                .nofJetActuations(1L)
//                .nofColdStartups(1L)
//                .inkUsageMl(1L)
//                .warmTimeHrs(1L)
//                .biasVoltageTimeHrs(1L)
//                .daysInUse(1L)
//                .build();
//
//        when(usageRepository.findAllByUsageDateBetween(date1, date2)).thenReturn(List.of(usageEntity1));
//
//        GetUsageResponse response1 = usageService.getUsageByDate(date1, date2);
//
//        Usage usage1 = Usage.builder()
//                .usageId(1L)
//                .printer(1L)
//                .usageDate(LocalDate.of(2020, 2, 2))
//                .positionInEngineId(1L)
//                .printHeadId(1L)
//                .installTimeStamp(LocalDate.of(2020, 2, 1))
//                .lastUsedTimeStamp(LocalDate.of(2020, 2, 1))
//                .nofSquareMmPrinted(1L)
//                .nofVibrationActions(1L)
//                .nofJetActuations(1L)
//                .nofColdStartups(1L)
//                .inkUsageMl(1L)
//                .warmTimeHrs(1L)
//                .biasVoltageTimeHrs(1L)
//                .daysInUse(1L)
//                .build();
//
//        GetUsageResponse expectedResponse1 = GetUsageResponse.builder()
//                .printHeadUsage(List.of(usage1))
//                .build();
//
//        assertEquals(expectedResponse1, response1);
//    }
}