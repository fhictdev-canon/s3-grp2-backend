package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.business.NozzleLogService;
import com.canon.printhead_health.domain.GetNozzleLogResponse;
import com.canon.printhead_health.domain.NozzleLog;
import com.canon.printhead_health.persistence.entity.NozzleLogEntity;
import com.canon.printhead_health.persistence.repository.NozzleLogRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetNozzleLogUseCaseImplTest {

//    @Mock
//    private NozzleLogRepository nozzleLogRepositoryMock;
//
//    @InjectMocks
//    private NozzleLogServiceImpl nozzleLogService;
//
//    @Test
//    void getNozzleLogs() {
//
//        NozzleLogEntity nozzleLogEntity1 = NozzleLogEntity.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2016,9,6,12,45))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(1L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//
//        NozzleLogEntity nozzleLogEntity2 = NozzleLogEntity.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 2))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 2")
//                .chip(2L)
//                .build();
//
//        when(nozzleLogRepositoryMock.findAll()).thenReturn(List.of(nozzleLogEntity1, nozzleLogEntity2));
//
//        GetNozzleLogResponse response = nozzleLogService.getNozzleLogs();
//
//        NozzleLog nozzleLog1 = NozzleLog.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2016,9,6,12,45))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(1L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//
//        NozzleLog nozzleLog2 = NozzleLog.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 2))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 2")
//                .chip(2L)
//                .build();
//
//        GetNozzleLogResponse expectedResponse = GetNozzleLogResponse.builder()
//                .nozzleLogs(List.of(nozzleLog1, nozzleLog2))
//                .build();
//
//        assertEquals(expectedResponse, response);
//
//        verify(nozzleLogRepositoryMock).findAll();
//    }
//
//    @Test
//    void shouldGetNozzleByNozzle() {
//        NozzleLogEntity nozzleLogEntity1 = NozzleLogEntity.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(2L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//        NozzleLogEntity nozzleLogEntity2 = NozzleLogEntity.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 2))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 2")
//                .chip(2L)
//                .build();
//        NozzleLogEntity nozzleLogEntity3 = NozzleLogEntity.builder()
//                .nozzleLogId(3L)
//                .printer(3L)
//                .timestamp(LocalDateTime.of(2020, 2, 3))
//                .engineCycleId(3L)
//                .swathId(3L)
//                .purpose("purpose test 3")
//                .nozzle(3L)
//                .error("error test 3")
//                .chip(3L)
//                .build();
//
//        when(nozzleLogRepositoryMock.findAllByNozzle(2L)).thenReturn(List.of(nozzleLogEntity1, nozzleLogEntity2));
//        when(nozzleLogRepositoryMock.findAllByNozzle(3L)).thenReturn(List.of(nozzleLogEntity3));
//
//        NozzleLog nozzleLog1 = NozzleLog.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(2L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//        NozzleLog nozzleLog2 = NozzleLog.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 2))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 2")
//                .chip(2L)
//                .build();
//        NozzleLog nozzleLog3 = NozzleLog.builder()
//                .nozzleLogId(3L)
//                .printer(3L)
//                .timestamp(LocalDateTime.of(2020, 2, 3))
//                .engineCycleId(3L)
//                .swathId(3L)
//                .purpose("purpose test 3")
//                .nozzle(3L)
//                .error("error test 3")
//                .chip(3L)
//                .build();
//
//        GetNozzleLogResponse expectedResponse1 = GetNozzleLogResponse.builder()
//                .nozzleLogs(List.of(nozzleLog1, nozzleLog2))
//                .build();
//        GetNozzleLogResponse expectedResponse2 = GetNozzleLogResponse.builder()
//                .nozzleLogs(List.of(nozzleLog3))
//                .build();
//
//        GetNozzleLogResponse actualResponse1 = nozzleLogService.getNozzleByNozzle(2L);
//        GetNozzleLogResponse actualResponse2 = nozzleLogService.getNozzleByNozzle(3L);
//
//        assertEquals(expectedResponse1, actualResponse1);
//        assertEquals(expectedResponse2, actualResponse2);
//
//        verify(nozzleLogRepositoryMock).findAllByNozzle(2L);
//        verify(nozzleLogRepositoryMock).findAllByNozzle(3L);
//    }
//
//    @Test
//    void shouldGetNozzleByError() {
//        NozzleLogEntity nozzleLogEntity1 = NozzleLogEntity.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(1L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//        NozzleLogEntity nozzleLogEntity2 = NozzleLogEntity.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 2))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 1")
//                .chip(2L)
//                .build();
//        NozzleLogEntity nozzleLogEntity3 = NozzleLogEntity.builder()
//                .nozzleLogId(3L)
//                .printer(3L)
//                .timestamp(LocalDateTime.of(2020, 2, 3))
//                .engineCycleId(3L)
//                .swathId(3L)
//                .purpose("purpose test 3")
//                .nozzle(3L)
//                .error("error test 3")
//                .chip(3L)
//                .build();
//
//        when(nozzleLogRepositoryMock.findAllByError("error test 1")).thenReturn(List.of(nozzleLogEntity1, nozzleLogEntity2));
//        when(nozzleLogRepositoryMock.findAllByError("error test 3")).thenReturn(List.of(nozzleLogEntity3));
//
//        NozzleLog nozzleLog1 = NozzleLog.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(1L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//        NozzleLog nozzleLog2 = NozzleLog.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 2))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 1")
//                .chip(2L)
//                .build();
//        NozzleLog nozzleLog3 = NozzleLog.builder()
//                .nozzleLogId(3L)
//                .printer(3L)
//                .timestamp(LocalDateTime.of(2020, 2, 3))
//                .engineCycleId(3L)
//                .swathId(3L)
//                .purpose("purpose test 3")
//                .nozzle(3L)
//                .error("error test 3")
//                .chip(3L)
//                .build();
//
//        GetNozzleLogResponse expectedResponse1 = GetNozzleLogResponse.builder()
//                .nozzleLogs(List.of(nozzleLog1, nozzleLog2))
//                .build();
//        GetNozzleLogResponse expectedResponse2 = GetNozzleLogResponse.builder()
//                .nozzleLogs(List.of(nozzleLog3))
//                .build();
//
//        GetNozzleLogResponse actualResponse1 = nozzleLogService.getNozzleLogByError("error test 1");
//        GetNozzleLogResponse actualResponse2 = nozzleLogService.getNozzleLogByError("error test 3");
//
//        assertEquals(expectedResponse1, actualResponse1);
//        assertEquals(expectedResponse2, actualResponse2);
//
//        verify(nozzleLogRepositoryMock).findAllByError("error test 1");
//        verify(nozzleLogRepositoryMock).findAllByError("error test 3");
//    }
//
//    @Test
//    void shouldGetNozzleLogById() {
//        NozzleLogEntity nozzleLogEntity1 = NozzleLogEntity.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(1L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//        NozzleLogEntity nozzleLogEntity2 = NozzleLogEntity.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 2))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 2")
//                .chip(2L)
//                .build();
//
//        when(nozzleLogRepositoryMock.findById(1L)).thenReturn(java.util.Optional.of(nozzleLogEntity1));
//        when(nozzleLogRepositoryMock.findById(2L)).thenReturn(java.util.Optional.of(nozzleLogEntity2));
//
//        NozzleLog nozzleLog1 = NozzleLog.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(1L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//        NozzleLog nozzleLog2 = NozzleLog.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 2))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 2")
//                .chip(2L)
//                .build();
//
//        GetNozzleLogResponse expectedResponse1 = GetNozzleLogResponse.builder()
//                .nozzleLogs(List.of(nozzleLog1))
//                .build();
//        GetNozzleLogResponse expectedResponse2 = GetNozzleLogResponse.builder()
//                .nozzleLogs(List.of(nozzleLog2))
//                .build();
//
//        GetNozzleLogResponse actualResponse1 = nozzleLogService.getNozzleLogById(1L);
//        GetNozzleLogResponse actualResponse2 = nozzleLogService.getNozzleLogById(2L);
//
//        assertEquals(expectedResponse1, actualResponse1);
//        assertEquals(expectedResponse2, actualResponse2);
//
//        verify(nozzleLogRepositoryMock).findById(1L);
//        verify(nozzleLogRepositoryMock).findById(2L);
//    }
//
//    @Test
//    void shouldGetNozzleLogsByDate() {
//        NozzleLogEntity nozzleLogEntity1 = NozzleLogEntity.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(1L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//        NozzleLogEntity nozzleLogEntity2 = NozzleLogEntity.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 2")
//                .chip(2L)
//                .build();
//
//        LocalDateTime firstTimestamp = LocalDateTime.of(2020, 1, 31);
//        LocalDateTime secondTimestamp = LocalDateTime.of(2020, 2, 2);
//
//        when(nozzleLogRepositoryMock.findAllByTimestampBetween(firstTimestamp, secondTimestamp)).thenReturn(List.of(nozzleLogEntity1, nozzleLogEntity2));
//
//        NozzleLog nozzleLog1 = NozzleLog.builder()
//                .nozzleLogId(1L)
//                .printer(1L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(1L)
//                .swathId(1L)
//                .purpose("purpose test 1")
//                .nozzle(1L)
//                .error("error test 1")
//                .chip(1L)
//                .build();
//        NozzleLog nozzleLog2 = NozzleLog.builder()
//                .nozzleLogId(2L)
//                .printer(2L)
//                .timestamp(LocalDateTime.of(2020, 2, 1))
//                .engineCycleId(2L)
//                .swathId(2L)
//                .purpose("purpose test 2")
//                .nozzle(2L)
//                .error("error test 2")
//                .chip(2L)
//                .build();
//
//        GetNozzleLogResponse expectedResponse = GetNozzleLogResponse.builder()
//                .nozzleLogs(List.of(nozzleLog1, nozzleLog2))
//                .build();
//
//        GetNozzleLogResponse actualResponse = nozzleLogService.getNozzleLogsByDate(LocalDateTime.of(2020, 2, 1));
//
//        assertEquals(expectedResponse, actualResponse);
//
//        verify(nozzleLogRepositoryMock).findAllByTimestampBetween(firstTimestamp, secondTimestamp);
//    }
}