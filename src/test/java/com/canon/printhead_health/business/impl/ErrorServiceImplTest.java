package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.domain.Error;
import com.canon.printhead_health.domain.GetErrorResponse;
import com.canon.printhead_health.persistence.entity.ErrorEntity;
import com.canon.printhead_health.persistence.repository.PrinterHeadErrorRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ErrorServiceImplTest {

//    @Mock
//    private PrinterHeadErrorRepository printerHeadErrorRepoMock;
//
//    @InjectMocks
//    private ErrorServiceImpl errorService;
//
//    @Test
//    void getPrinterHeadErrorById() {
//
//        ErrorEntity errorEntity1 = ErrorEntity.builder()
//                .id(1L)
//                .printer(1)
//                .timestamp(LocalDate.of(2020, 2, 1))
//                .printheadId(1)
//                .errorEventNr(1)
//                .errorCode(1)
//                .build();
//
//        ErrorEntity errorEntity2 = ErrorEntity.builder()
//                .id(2L)
//                .printer(2)
//                .timestamp(LocalDate.of(2020, 2, 2))
//                .printheadId(2)
//                .errorEventNr(2)
//                .errorCode(2)
//                .build();
//
//        when(printerHeadErrorRepoMock.findById(1L)).thenReturn(java.util.Optional.of(errorEntity1));
//        when(printerHeadErrorRepoMock.findById(2L)).thenReturn(java.util.Optional.of(errorEntity2));
//
//        GetErrorResponse response1 = errorService.getPrinterHeadErrorById(1L);
//        GetErrorResponse response2 = errorService.getPrinterHeadErrorById(2L);
//
//        Error error1 = Error.builder()
//                .id(1L)
//                .printer(1)
//                .timestamp(LocalDate.of(2020, 2, 1))
//                .printheadId(1)
//                .errorEventNr(1)
//                .errorCode(1)
//                .build();
//
//        Error error2 = Error.builder()
//                .id(2L)
//                .printer(2)
//                .timestamp(LocalDate.of(2020, 2, 2))
//                .printheadId(2)
//                .errorEventNr(2)
//                .errorCode(2)
//                .build();
//
//        GetErrorResponse expectedResponse1 = GetErrorResponse.builder()
//                .errors(List.of(error1))
//                .build();
//
//        GetErrorResponse expectedResponse2 = GetErrorResponse.builder()
//                .errors(List.of(error2))
//                .build();
//
//        assertEquals(expectedResponse1, response1);
//        assertEquals(expectedResponse2, response2);
//
//
//    }
//
//    @Test
//    void getAllPrinterHeadErrors() {
//
//        ErrorEntity errorEntity1 = ErrorEntity.builder()
//                .id(1L)
//                .printer(1)
//                .timestamp(LocalDate.of(2020, 2, 1))
//                .printheadId(1)
//                .errorEventNr(1)
//                .errorCode(1)
//                .build();
//
//        ErrorEntity errorEntity2 = ErrorEntity.builder()
//                .id(2L)
//                .printer(2)
//                .timestamp(LocalDate.of(2020, 2, 2))
//                .printheadId(2)
//                .errorEventNr(2)
//                .errorCode(2)
//                .build();
//
//        ErrorEntity errorEntity3 = ErrorEntity.builder()
//                .id(3L)
//                .printer(3)
//                .timestamp(LocalDate.of(2020, 2, 3))
//                .printheadId(3)
//                .errorEventNr(3)
//                .errorCode(3)
//                .build();
//
//        when(printerHeadErrorRepoMock.findAll()).thenReturn(List.of(errorEntity1, errorEntity2, errorEntity3));
//
//        GetErrorResponse response = errorService.getAllPrinterHeadErrors();
//
//        Error error1 = Error.builder()
//                .id(1L)
//                .printer(1)
//                .timestamp(LocalDate.of(2020, 2, 1))
//                .printheadId(1)
//                .errorEventNr(1)
//                .errorCode(1)
//                .build();
//
//        Error error2 = Error.builder()
//                .id(2L)
//                .printer(2)
//                .timestamp(LocalDate.of(2020, 2, 2))
//                .printheadId(2)
//                .errorEventNr(2)
//                .errorCode(2)
//                .build();
//
//        Error error3 = Error.builder()
//                .id(3L)
//                .printer(3)
//                .timestamp(LocalDate.of(2020, 2, 3))
//                .printheadId(3)
//                .errorEventNr(3)
//                .errorCode(3)
//                .build();
//
//        GetErrorResponse expectedResponse = GetErrorResponse.builder()
//                .errors(List.of(error1, error2, error3))
//                .build();
//
//        assertEquals(expectedResponse, response);
//
//        verify(printerHeadErrorRepoMock).findAll();
//
//    }
//
//    @Test
//    void filterErrorsByDate() {
//
//        ErrorEntity errorEntity1 = ErrorEntity.builder()
//                .id(1L)
//                .printer(1)
//                .timestamp(LocalDate.of(2020, 2, 1))
//                .printheadId(1)
//                .errorEventNr(1)
//                .errorCode(1)
//                .build();
//
//        ErrorEntity errorEntity2 = ErrorEntity.builder()
//                .id(2L)
//                .printer(2)
//                .timestamp(LocalDate.of(2020, 2, 2))
//                .printheadId(2)
//                .errorEventNr(2)
//                .errorCode(2)
//                .build();
//
//        ErrorEntity errorEntity3 = ErrorEntity.builder()
//                .id(3L)
//                .printer(3)
//                .timestamp(LocalDate.of(2020, 2, 3))
//                .printheadId(3)
//                .errorEventNr(3)
//                .errorCode(3)
//                .build();
//
//        ErrorEntity errorEntity4 = ErrorEntity.builder()
//                .id(4L)
//                .printer(4)
//                .timestamp(LocalDate.of(2020, 2, 4))
//                .printheadId(4)
//                .errorEventNr(4)
//                .errorCode(4)
//                .build();
//
//        ErrorEntity errorEntity5 = ErrorEntity.builder()
//                .id(5L)
//                .printer(5)
//                .timestamp(LocalDate.of(2020, 2, 5))
//                .printheadId(5)
//                .errorEventNr(5)
//                .errorCode(5)
//                .build();
//
//        when(printerHeadErrorRepoMock.findAll()).thenReturn(List.of(errorEntity1, errorEntity2, errorEntity3, errorEntity4, errorEntity5));
//
//        GetErrorResponse response = errorService.filterErrorsByDate(LocalDate.of(2020, 2, 3), LocalDate.of(2020, 2, 5));
//
//        Error error3 = Error.builder()
//                .id(3L)
//                .printer(3)
//                .timestamp(LocalDate.of(2020, 2, 3))
//                .printheadId(3)
//                .errorEventNr(3)
//                .errorCode(3)
//                .build();
//
//        Error error4 = Error.builder()
//                .id(4L)
//                .printer(4)
//                .timestamp(LocalDate.of(2020, 2, 4))
//                .printheadId(4)
//                .errorEventNr(4)
//                .errorCode(4)
//                .build();
//
//        Error error5 = Error.builder()
//                .id(5L)
//                .printer(5)
//                .timestamp(LocalDate.of(2020, 2, 5))
//                .printheadId(5)
//                .errorEventNr(5)
//                .errorCode(5)
//                .build();
//
//        GetErrorResponse expectedResponse = GetErrorResponse.builder()
//                .errors(List.of(error3, error4, error5))
//                .build();
//
//        assertEquals(expectedResponse, response);
//    }


}
