package com.canon.printhead_health.business;

import com.canon.printhead_health.domain.GetErrorResponse;
import com.canon.printhead_health.domain.GetNozzleLogResponse;
import com.canon.printhead_health.domain.ResponseData;
import com.canon.printhead_health.domain.ResponseDataNozzleLog;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

public interface NozzleLogService {
    GetNozzleLogResponse getNozzleLogs(Long printer);

    GetNozzleLogResponse getNozzleLogById(Long id);

    GetNozzleLogResponse getNozzleByNozzle(Long nozzle, Long printer);

    GetNozzleLogResponse getNozzleLogsByDate(LocalDateTime date, Long printer);

    GetNozzleLogResponse getNozzleLogByError(String error, Long printer);

    ResponseDataNozzleLog findByErrorAndDateAndPrinter(String error, LocalDateTime nozzleLogDateStart, LocalDateTime nozzleLogDateEnd, Long printer);


    GetNozzleLogResponse getNozzleLogByAllFilters(Long nozzle,String error, LocalDateTime first, LocalDateTime Second, Long printer);
    GetNozzleLogResponse getNozzleLogByAllFilters(Long nozzleStart,Long nozzleEnd, String error, LocalDateTime first, LocalDateTime Second, Long printer);


    void save(MultipartFile file);

    Boolean isUpdated();

    void updateTrue();

    void updateFalse();

}
