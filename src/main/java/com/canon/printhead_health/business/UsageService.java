package com.canon.printhead_health.business;

import com.canon.printhead_health.domain.GetUsageDIUResponse;
import com.canon.printhead_health.domain.GetUsageResponse;
import com.canon.printhead_health.domain.ResponseData;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

public interface UsageService {
    GetUsageResponse getUsageById(Long id);

    GetUsageResponse getUsageByDate(LocalDate startDate, LocalDate endDate, Long printer);


    void save(MultipartFile file);

    Boolean isUpdated();

    ResponseData findByParamAndDate(String paramString, LocalDate usageDateStart, LocalDate usageDateEnd, Long printer, Long printHeadId);

    GetUsageDIUResponse getDaysInUse(LocalDate startDate, LocalDate endDate, Long printHeadId, Long printer);
}
