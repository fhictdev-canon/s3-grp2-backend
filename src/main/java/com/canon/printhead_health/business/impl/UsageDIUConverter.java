package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.domain.UsageDIU;
import com.canon.printhead_health.persistence.result.DaysInUseResults;

final class UsageDIUConverter {

    public static UsageDIU convert(DaysInUseResults usageEntity) {
        return UsageDIU.builder()
                .daysInUse(usageEntity.getDaysInUse())
                .build();
    }
}
