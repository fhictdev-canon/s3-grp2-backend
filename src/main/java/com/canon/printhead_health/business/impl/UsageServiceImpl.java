package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.business.UsageService;
import com.canon.printhead_health.domain.GetUsageDIUResponse;
import com.canon.printhead_health.domain.GetUsageResponse;
import com.canon.printhead_health.domain.ResponseData;
import com.canon.printhead_health.domain.Usage;
import com.canon.printhead_health.persistence.entity.UsageEntity;
import com.canon.printhead_health.domain.UsageDIU;
import com.canon.printhead_health.persistence.repository.UsageRepository;
import com.canon.printhead_health.persistence.repository.updateStatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.canon.printhead_health.helper.CsvHelperPrintHeadUsage;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class UsageServiceImpl implements UsageService {

    @Autowired
    private UsageRepository usageRepository;
    private updateStatusRepository updateRepo;


    @Override
    public GetUsageResponse getUsageById(Long id) {
        List<Usage> usageByPrinterId = usageRepository.findById(id)
                .stream()
                .map(UsageConverter::convert)
                .toList();

        return GetUsageResponse.builder()
                .printHeadUsage(usageByPrinterId)
                .build();
    }

    @Override
    public GetUsageResponse getUsageByDate(LocalDate startDate, LocalDate endDate, Long printer) {

        if(printer!=null){
            List<Usage> usageByDate = usageRepository.findAllByUsageDateBetweenAndPrinter(startDate, endDate, printer)
                    .stream()
                    .map(UsageConverter::convert)
                    .toList();

            return GetUsageResponse.builder()
                    .printHeadUsage(usageByDate)
                    .build();
        }else {
            List<Usage> usageByDate = usageRepository.findAllByUsageDateBetween(startDate, endDate)
                    .stream()
                    .map(UsageConverter::convert)
                    .toList();

            return GetUsageResponse.builder()
                    .printHeadUsage(usageByDate)
                    .build();
        }
    }

    public void save(MultipartFile file) {
        try {
            List<UsageEntity> usages = CsvHelperPrintHeadUsage.csvToTutorials(file.getInputStream());
            usageRepository.saveAll(usages);
            updateTrue();
        } catch (Exception e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }


    public Boolean isUpdated() {

        System.out.println(updateRepo.findByErrorStatusTrue().size());
        if(updateRepo.findByUsageStatusTrue().size() > 0) {
            updateFalse();
            return true;
        }else {
            return false;
        }

    }

    private void updateTrue() {
        updateRepo.updateUsageStatusToTrue();
    }

    private void updateFalse() {
        updateRepo.updateUsageStatusToFalse();
    }

    @Override
    public ResponseData findByParamAndDate(String paramString, LocalDate usageDateStart, LocalDate usageDateEnd, Long printer, Long printHeadId) {
        List<Usage> usages;
        List<Float> x = new ArrayList<>();
        List<LocalDate> y = new ArrayList<>();

        usages = usageRepository.findAllByUsageDateBetweenAndPrinterAndPrintHeadId(usageDateStart, usageDateEnd, printer, printHeadId)
                .stream()
                .map(UsageConverter::convert)
                .toList();

        switch (paramString) {
            case "NrOfVibrations" -> {
                for (Usage usage : usages) {
                    x.add(usage.getNofVibrationActions().floatValue());
                    y.add(usage.getUsageDate());
                }
            }
            case "NrOfJetActuations" -> {
                for (Usage usage : usages) {
                    x.add(usage.getNofJetActuations().floatValue());
                    y.add(usage.getUsageDate());
                }
            }
            case "NrOfColdStartups" -> {
                for (Usage usage : usages) {
                    x.add(usage.getNofColdStartups().floatValue());
                    y.add(usage.getUsageDate());
                }
            }
            case "InkUsage" -> {
                for (Usage usage : usages) {
                    x.add(usage.getInkUsageMl().floatValue());
                    y.add(usage.getUsageDate());
                }
            }
            case "WarmTime" -> {
                for (Usage usage : usages) {
                    x.add(usage.getWarmTimeHrs().floatValue());
                    y.add(usage.getUsageDate());
                }
            }
            case "BiasVoltageTime" -> {
                for (Usage usage : usages) {
                    x.add(usage.getBiasVoltageTimeHrs().floatValue());
                    y.add(usage.getUsageDate());
                }
            }
            default -> throw new IllegalStateException("Unexpected value: " + paramString);
        }




        return ResponseData.builder()
                .x(x)
                .y(y)
                .build();
    }


    @Override
    public GetUsageDIUResponse getDaysInUse(LocalDate firstDate, LocalDate secondDate, Long printHeadId, Long printer) {

        List<UsageDIU> daysInUse = usageRepository.findAllByUsageDateAndPrintHeadIdAndPrinter(
                firstDate, secondDate, printHeadId, printer, PageRequest.of(0, 1))
                .stream()
                .map(UsageDIUConverter::convert)
                .toList();

        return GetUsageDIUResponse.builder()
                .usageDIU(daysInUse)
                .build();
    }
}