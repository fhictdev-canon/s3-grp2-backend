package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.helper.CsvHelperError;
import com.canon.printhead_health.business.ErrorService;
import com.canon.printhead_health.domain.*;
import com.canon.printhead_health.domain.Error;
import com.canon.printhead_health.persistence.entity.ErrorEntity;
import com.canon.printhead_health.persistence.repository.PrinterHeadErrorRepository;
import com.canon.printhead_health.persistence.repository.updateStatusRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;


@Service("ErrorService")
@AllArgsConstructor
public class ErrorServiceImpl implements ErrorService {

    private PrinterHeadErrorRepository printerHeadErrorRepository;
    private updateStatusRepository updateRepo;



    @Override
    public GetErrorResponse getPrinterHeadErrorById(Long id, Long printer) {

        if (printer != null) {
            List<Error> error = printerHeadErrorRepository.findByIdAndPrinter(id, printer)
                    .stream()
                    .map(ErrorConverter::convert)
                    .toList();

            return GetErrorResponse.builder()
                    .errors(error)
                    .build();
        } else {
            List<Error> error = printerHeadErrorRepository.findById(id)
                    .stream()
                    .map(ErrorConverter::convert)
                    .toList();

            return GetErrorResponse.builder()
                    .errors(error)
                    .build();
        }
    }

    @Override
    public GetErrorResponse getAllPrinterHeadErrors(Long printer) {
        if (printer != null) {
            List<Error> errors = printerHeadErrorRepository.findAllByPrinter(printer)
                    .stream()
                    .map(ErrorConverter::convert)
                    .toList();
            return GetErrorResponse.builder()
                    .errors(errors)
                    .build();
        } else {
            List<Error> errors = printerHeadErrorRepository.findAll()
                    .stream()
                    .map(ErrorConverter::convert)
                    .toList();
            return GetErrorResponse.builder()
                    .errors(errors)
                    .build();
        }
    }

    @Override
    public GetErrorResponse filterErrorsByDate(LocalDate startDate, LocalDate endDate, Long printer) {

        if (printer != null) {
            List<Error> errors = printerHeadErrorRepository.findAllByTimestampBetweenAndPrinter(startDate, endDate, printer)
                    .stream()
                    .map(ErrorConverter::convert)
                    .toList();
            return GetErrorResponse.builder()
                    .errors(errors)
                    .build();
        } else {
            List<Error> errors = printerHeadErrorRepository.findAllByTimestampBetween(startDate, endDate)
                    .stream()
                    .map(ErrorConverter::convert)
                    .toList();
            return GetErrorResponse.builder()
                    .errors(errors)
                    .build();
        }
    }

    @Override
    public GetErrorCodeResponse getTop10Errors(LocalDate startDate, LocalDate endDate, Long printer) {

            if(printer!=null){
                List<ErrorCodeList> errors = printerHeadErrorRepository.countByErrorCodeAndPrinter(startDate, endDate, printer, PageRequest.of(0, 10))
                        .stream()
                        .map(ErrorCodeConverter::convert)
                        .toList();
                return GetErrorCodeResponse.builder()
                        .errors(errors)
                        .build();
            } else {
                List<ErrorCodeList> errors = printerHeadErrorRepository.countByErrorCode(startDate, endDate, PageRequest.of(0, 10))
                        .stream()
                        .map(ErrorCodeConverter::convert)
                        .toList();
                return GetErrorCodeResponse.builder()
                        .errors(errors)
                        .build();
            }
    }


    @Override
    public GetErrorResponse getErrorByPrinter(Long printer) {
        List<Error> errors = printerHeadErrorRepository.findAllByPrinter(printer)
                .stream()
                .map(ErrorConverter::convert)
                .toList();
        return GetErrorResponse.builder()
                .errors(errors)
                .build();
    }


    public void save(MultipartFile file) {
        try {
            List<ErrorEntity> tutorials = CsvHelperError.csvToTutorials(file.getInputStream());
            updateTrue();
            printerHeadErrorRepository.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }

    public Boolean isUpdated()
    {
        if(updateRepo.findByErrorStatusTrue().size()>0) {
            System.out.println(updateRepo.findByErrorStatusTrue().size());
            updateFalse();
            return true;
        }else{
            return false;
        }
    }

    public void updateTrue(){
            updateRepo.updateErrorStatusToTrue();
        }
    public void updateFalse(){
            updateRepo.updateErrorStatusToFalse();

        }

    @Override
    public Long getNumberOfPrinters() {
        return printerHeadErrorRepository.countDistinctByPrinter();
    }


}