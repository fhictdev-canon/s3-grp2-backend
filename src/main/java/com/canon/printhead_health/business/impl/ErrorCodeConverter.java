package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.domain.Error;
import com.canon.printhead_health.domain.ErrorCodeList;
import com.canon.printhead_health.persistence.entity.ErrorEntity;
import com.canon.printhead_health.persistence.result.ErrorCountResults;

public class ErrorCodeConverter {


    public static ErrorCodeList convert(ErrorCountResults errorEntity) {
        return ErrorCodeList.builder()
                .codeError(errorEntity.getErrorCode())
                .codeCodesUsage(errorEntity.getErrorCodesUsage())
                .printer(errorEntity.getPrinter())
                .build();
    }
}
