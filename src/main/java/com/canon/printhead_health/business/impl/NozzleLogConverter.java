package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.domain.NozzleLog;
import com.canon.printhead_health.persistence.entity.NozzleLogEntity;

final class NozzleLogConverter {

    private NozzleLogConverter() {
    }

    public static NozzleLog convert(NozzleLogEntity nozzleLogEntity) {
            return NozzleLog.builder()
                    .nozzleLogId(nozzleLogEntity.getNozzleLogId())
                    .printer(nozzleLogEntity.getPrinter())
                    .timestamp(nozzleLogEntity.getTimestamp())
                    .engineCycleId(nozzleLogEntity.getEngineCycleId())
                    .swathId(nozzleLogEntity.getSwathId())
                    .purpose(nozzleLogEntity.getPurpose())
                    .nozzle(nozzleLogEntity.getNozzle())
                    .error(nozzleLogEntity.getError())
                    .chip(nozzleLogEntity.getChip())
                    .build();
    }

}
