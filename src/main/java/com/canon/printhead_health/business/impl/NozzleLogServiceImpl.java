package com.canon.printhead_health.business.impl;


import com.canon.printhead_health.business.NozzleLogService;
import com.canon.printhead_health.domain.*;
import com.canon.printhead_health.domain.Error;
import com.canon.printhead_health.domain.GetErrorResponse;
import com.canon.printhead_health.domain.GetNozzleLogResponse;
import com.canon.printhead_health.domain.NozzleLog;
import com.canon.printhead_health.helper.CsvHelperError;
import com.canon.printhead_health.helper.CsvHelperNozzleLog;
import com.canon.printhead_health.persistence.entity.ErrorEntity;
import com.canon.printhead_health.persistence.entity.NozzleLogEntity;
import com.canon.printhead_health.persistence.repository.NozzleLogRepository;
import com.canon.printhead_health.persistence.repository.updateStatusRepository;
import lombok.AllArgsConstructor;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class NozzleLogServiceImpl implements NozzleLogService {

    @Autowired
    NozzleLogRepository repo;
    private updateStatusRepository updateRepo;


    @Override
    public GetNozzleLogResponse getNozzleLogs(Long printer) {
        if (printer != null) {
            List<NozzleLog> nozzleLogs = repo.findAllByPrinter(printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();

            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzleLogs)
                    .build();
        }else{
            List<NozzleLog> nozzleLogs = findAll()
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();

        return GetNozzleLogResponse.builder()
                .nozzleLogs(nozzleLogs)
                .build();
    }
    }

    private List<NozzleLogEntity> findAll() {
        return repo.findAll();
    }



    @Override
    public GetNozzleLogResponse getNozzleLogsByDate(LocalDateTime timestamp, Long printer){
        LocalDateTime first = timestamp; //Between works in a weird way where it takes the first variable as => but the second as < or something
        LocalDateTime second = timestamp.plusDays(1);

        if(printer!=null){
            List<NozzleLog> NozzleLogTimestamp = repo.findAllByTimestampBetweenAndPrinter(first, second, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(NozzleLogTimestamp)
                    .build();
        }else{
            List<NozzleLog> NozzleLogTimestamp = repo.findAllByTimestampBetween(first, second)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(NozzleLogTimestamp)
                    .build();
        }

    }

    @Override
    public GetNozzleLogResponse getNozzleByNozzle(Long nozzle, Long printer){

        if(printer!=null){
            List<NozzleLog> nozzleNr = repo.findAllByNozzleAndPrinter(nozzle, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();

            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzleNr)
                    .build();

        }else {
            List<NozzleLog> nozzleNr = repo.findAllByNozzle(nozzle)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();

            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzleNr)
                    .build();
        }
    }

    @Override
    public GetNozzleLogResponse getNozzleLogByError(String Error, Long printer){
        List<NozzleLog> NozzleLogError;
        if(printer!=null) {
            NozzleLogError = repo.findAllByErrorAndPrinter(Error, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
        } else{
            NozzleLogError = repo.findAllByError(Error)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
        }
        return GetNozzleLogResponse.builder()
                .nozzleLogs(NozzleLogError)
                .build();
    }

    @Override
    public GetNozzleLogResponse getNozzleLogById(Long id) {
        List<NozzleLog> error = repo.findById(id)
                .stream()
                .map(NozzleLogConverter::convert)
                .toList();

        return GetNozzleLogResponse.builder()
                .nozzleLogs(error)
                .build();
    }

    @Override
    public ResponseDataNozzleLog findByErrorAndDateAndPrinter(String error, LocalDateTime nozzleLogDateStart, LocalDateTime nozzleLogDateEnd, Long printer) {
        List<NozzleLog> nozzleLogs;
        List<Long> x = new ArrayList<>();
        List<LocalDateTime> y = new ArrayList<>();
        List<NozzleLogXYData> nozzleLogsData = new ArrayList<>();

        nozzleLogs = repo.findAllByTimestampBetweenAndPrinterAndError(nozzleLogDateStart, nozzleLogDateEnd, printer, error)
                .stream()
                .map(NozzleLogConverter::convert)
                .toList();

        for (NozzleLog nozzleLog : nozzleLogs) {

            LocalDateTime localDateTime = nozzleLog.getTimestamp();

            ZonedDateTime zdt = ZonedDateTime.of(localDateTime, ZoneId.systemDefault());
            long date = zdt.toInstant().toEpochMilli();

            NozzleLogXYData nozzle = NozzleLogXYData.builder()
                    .x(date)
                    .y(nozzleLog.getNozzle())
                    .build();
            nozzleLogsData.add(nozzle);;
        }

        return ResponseDataNozzleLog.builder()
                .nozzleLogXYData(nozzleLogsData)
                .build();
    }

    public void save(MultipartFile file) {
        try {
            List<NozzleLogEntity> tutorials = CsvHelperNozzleLog.csvToTutorials(file.getInputStream());
            updateTrue();
            repo.saveAll(tutorials);
        } catch (IOException e) {
            throw new RuntimeException("fail to store csv data: " + e.getMessage());
        }
    }
    public void updateTrue(){
        updateRepo.updateNozzleStatusToTrue();
    }
    public void updateFalse(){
        updateRepo.updateNozzleStatusToFalse();
    }

    public Boolean isUpdated()
    {
        if(updateRepo.findByNozzleStatusTrue().size()>0) {
            System.out.println(updateRepo.findByNozzleStatusTrue().size());
            updateFalse();
            return true;
        }else{
            return false;
        }
    }




    // Not even worth trying to read this lol
    @Override
    public GetNozzleLogResponse getNozzleLogByAllFilters(Long nozzle, String error, LocalDateTime first, LocalDateTime second, Long printer){
        //nozzle,error,date
        /*
                List<NozzleLog> nozzles =
                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
         */


        if(nozzle !=null && error !=null & first != null){
            List<NozzleLog> nozzles = repo.findAllByTimestampBetweenAndNozzleAndErrorAndPrinter(first, second, nozzle, error,printer)
                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();

        }
        else if(nozzle !=null && error !=null){
            List<NozzleLog> nozzles = repo.findAllByNozzleAndErrorAndPrinter(nozzle, error, printer)
                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }else if(nozzle !=null && first !=null){
            List<NozzleLog> nozzles = repo.findAllByTimestampBetweenAndNozzleAndPrinter(first, second, nozzle, printer)
                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }else if(nozzle !=null ){
            List<NozzleLog> nozzles = repo.findAllByNozzleAndPrinter(nozzle, printer)
                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }
        else if(error!=null && first!=null) {
            List<NozzleLog> nozzles = repo.findAllByTimestampBetweenAndErrorAndPrinter(first, second, error, printer)
                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }else if(error!=null){
            List<NozzleLog> nozzles = repo.findAllByErrorAndPrinter(error, printer)

                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }else if(first!=null){
            List<NozzleLog> nozzles = repo.findAllByTimestampBetweenAndPrinter(first, second, printer)
                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }
        return null;
    }
    @Override
    public GetNozzleLogResponse getNozzleLogByAllFilters(Long nozzle, Long nozzle2, String error, LocalDateTime first, LocalDateTime second, Long printer) {
        //nozzle,error,date
        /*
                List<NozzleLog> nozzles =
                .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
         */



        if (nozzle != null && error != null & first != null) {
            List<NozzleLog> nozzles = repo.findAllByTimestampBetweenAndNozzleBetweenAndErrorAndPrinter(first, second, nozzle,nozzle2, error, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();

        }else if(nozzle !=null && error !=null){
            List<NozzleLog> nozzles = repo.findAllByNozzleBetweenAndErrorAndPrinter(nozzle,nozzle2, error, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }else if(nozzle !=null && first !=null){
            List<NozzleLog> nozzles = repo.findAllByTimestampBetweenAndNozzleBetweenAndPrinter(first, second, nozzle,nozzle2, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }else if(nozzle !=null ){
            List<NozzleLog> nozzles = repo.findAllByNozzleBetweenAndPrinter(nozzle,nozzle2, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }
        else if(error!=null && first!=null) {
            List<NozzleLog> nozzles = repo.findAllByTimestampBetweenAndErrorAndPrinter(first, second, error, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }else if(error!=null){
            List<NozzleLog> nozzles = repo.findAllByErrorAndPrinter(error, printer)

                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }else if(first!=null){
            List<NozzleLog> nozzles = repo.findAllByTimestampBetweenAndPrinter(first, second, printer)
                    .stream()
                    .map(NozzleLogConverter::convert)
                    .toList();
            return GetNozzleLogResponse.builder()
                    .nozzleLogs(nozzles)
                    .build();
        }

        return null;
    }



}
