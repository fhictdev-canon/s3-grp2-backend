package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.domain.Usage;
import com.canon.printhead_health.persistence.entity.UsageEntity;

final class UsageConverter {

    public static Usage convert(UsageEntity usageEntity) {
        return Usage.builder()
                .usageId(usageEntity.getUsageId())
                .printer(usageEntity.getPrinter())
                .usageDate(usageEntity.getUsageDate())
                .positionInEngineId(usageEntity.getPositionInEngineId())
                .printHeadId(usageEntity.getPrintHeadId())
                .installTimeStamp(usageEntity.getInstallTimeStamp())
                .lastUsedTimeStamp(usageEntity.getLastUsedTimeStamp())
                .nofSquareMmPrinted(usageEntity.getNofSquareMmPrinted())
                .nofVibrationActions(usageEntity.getNofVibrationActions())
                .nofJetActuations(usageEntity.getNofJetActuations())
                .nofColdStartups(usageEntity.getNofColdStartups())
                .inkUsageMl(usageEntity.getInkUsageMl())
                .warmTimeHrs(usageEntity.getWarmTimeHrs())
                .biasVoltageTimeHrs(usageEntity.getBiasVoltageTimeHrs())
                .daysInUse(usageEntity.getDaysInUse())
                .build();
    }
}
