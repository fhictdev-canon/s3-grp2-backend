package com.canon.printhead_health.business.impl;

import com.canon.printhead_health.domain.Error;
import com.canon.printhead_health.persistence.entity.ErrorEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class ErrorConverter {


    public static Error convert(ErrorEntity errorEntity) {
        return Error.builder()
                .id(errorEntity.getId())
                .printer(errorEntity.getPrinter())
                .timestamp(errorEntity.getTimestamp())
                .printheadId(errorEntity.getPrintheadId())
                .errorEventNr(errorEntity.getErrorEventNr())
                .errorCode(errorEntity.getErrorCode())
                .build();
    }
}
