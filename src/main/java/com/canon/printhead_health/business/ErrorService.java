package com.canon.printhead_health.business;

import com.canon.printhead_health.domain.GetErrorCodeResponse;
import com.canon.printhead_health.domain.GetErrorResponse;
import com.canon.printhead_health.domain.GetNozzleLogResponse;
import com.canon.printhead_health.domain.ResponseMessage;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

public interface ErrorService {

    GetErrorResponse getPrinterHeadErrorById(Long id, Long printer);

    GetErrorResponse getAllPrinterHeadErrors(Long printer);

    //public List<ErrorEntity> filterErrorsByDate(LocalDate startDate, LocalDate endDate)

    GetErrorResponse filterErrorsByDate(LocalDate startDate, LocalDate endDate, Long printer);

    GetErrorCodeResponse getTop10Errors(LocalDate startDate, LocalDate endDate, Long printer);

    GetErrorResponse getErrorByPrinter(Long printer);

    void save(MultipartFile file);

    Boolean isUpdated();

    void updateTrue();

    void updateFalse();


    Long getNumberOfPrinters();

}