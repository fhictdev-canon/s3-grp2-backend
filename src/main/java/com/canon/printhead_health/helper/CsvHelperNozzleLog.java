package com.canon.printhead_health.helper;

import com.canon.printhead_health.persistence.entity.ErrorEntity;
import com.canon.printhead_health.persistence.entity.NozzleLogEntity;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CsvHelperNozzleLog {
    public static String TYPE = "text/csv";
    static String[] HEADERs = {"nozzleId", "printer", "timestamp", "printheadId", "errorEventNr", "errorCode"};


    public static boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }


    public static List<NozzleLogEntity> csvToTutorials(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<NozzleLogEntity> nozzles = new ArrayList<NozzleLogEntity>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            for (CSVRecord csvRecord : csvRecords) {
                NozzleLogEntity nozzle = new NozzleLogEntity(
                        Long.parseLong(csvRecord.get("nozzleId")),
                        Long.parseLong(csvRecord.get("Printer")),
                        LocalDateTime.parse(csvRecord.get("Timestamp"), formatter),
                        Long.parseLong(csvRecord.get("EngineCycleId")),
                        Long.parseLong(csvRecord.get("SwathId")),
                        csvRecord.get("Purpose"),
                        Long.parseLong(csvRecord.get("Nozzle")),
                        csvRecord.get("Error"),
                        Long.parseLong(csvRecord.get("Chip"))
                );

                nozzles.add(nozzle);
            }
            return nozzles;

        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }
    }
}

