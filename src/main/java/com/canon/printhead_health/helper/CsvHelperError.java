package com.canon.printhead_health.helper;


import com.canon.printhead_health.persistence.entity.ErrorEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.Column;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import com.canon.printhead_health.domain.Error;


public class CsvHelperError {
    public static String TYPE = "text/csv";
    static String[] HEADERs = {"errorId", "printer", "timestamp", "printheadId", "errorEventNr", "errorCode"};


    public static boolean hasCSVFormat(MultipartFile file) {

        if (!TYPE.equals(file.getContentType())) {
            return false;
        }

        return true;
    }


    public static List<ErrorEntity> csvToTutorials(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
             CSVParser csvParser = new CSVParser(fileReader,
                     CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<ErrorEntity> errors = new ArrayList<ErrorEntity>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            for (CSVRecord csvRecord : csvRecords) {
                ErrorEntity error = new ErrorEntity(
                        Long.parseLong(csvRecord.get("errorId")),
                        Long.parseLong(csvRecord.get("printer")),
                        LocalDate.parse(csvRecord.get("timestamp"), formatter),
                        Integer.parseInt(csvRecord.get("printheadId")),
                        Integer.parseInt(csvRecord.get("errorEventNr")),
                        Integer.parseInt(csvRecord.get("errorCode"))
                );

                errors.add(error);
            }
            return errors;

        } catch (IOException e) {
            throw new RuntimeException("fail to parse CSV file: " + e.getMessage());
        }

    }
}
