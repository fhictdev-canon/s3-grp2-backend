package com.canon.printhead_health.helper;

import com.canon.printhead_health.persistence.entity.UsageEntity;
import org.springframework.web.multipart.MultipartFile;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class CsvHelperPrintHeadUsage {

    public static String TYPE = "text/csv";

    static String[] HEADERs = { "usageId", "printer", "usageDate", "positionEngine", "printHeadId", "installTimeStamp", "lastUsedTimeStamp", "nofSquareMilliMetersPrinted", "nofVibrationActuations", "nofJetActuations", "nofColdStartups", "inkUsageml", "warmTimehr", "biasVoltageTimehr", "daysInUse" };

    public static boolean hasCSVFormat(MultipartFile file) {
        if (!TYPE.equals(file.getContentType())) {
            return false;
        }
        return true;
    }

    public static List<UsageEntity> csvToTutorials(InputStream is) {
        try (BufferedReader fileReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                CSVParser csvParser = new CSVParser(fileReader,
                        CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {

            List<UsageEntity> usages = new ArrayList<UsageEntity>();

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            for (CSVRecord csvRecord : csvRecords) {
                UsageEntity usage = new UsageEntity(
                        Long.parseLong(csvRecord.get("usageId")),
                        Long.parseLong(csvRecord.get("printer")),
                        LocalDate.parse(csvRecord.get("usageDate"), formatter),
                        Long.parseLong(csvRecord.get("positionEngine")),
                        Long.parseLong(csvRecord.get("printHeadId")),
                        LocalDate.parse(csvRecord.get("installTimeStamp"), formatter),
                        LocalDate.parse(csvRecord.get("lastUsedTimeStamp"), formatter),
                        Long.parseLong(csvRecord.get("nofSquareMilliMetersPrinted")),
                        Long.parseLong(csvRecord.get("nofVibrationActuations")),
                        Long.parseLong(csvRecord.get("nofJetActuations")),
                        Long.parseLong(csvRecord.get("nofColdStartups")),
                        Long.parseLong(csvRecord.get("inkUsageml")),
                        Long.parseLong(csvRecord.get("warmTimehr")),
                        Long.parseLong(csvRecord.get("biasVoltageTimehr")),
                        Long.parseLong(csvRecord.get("daysInUse"))
                );
                usages.add(usage);
            }

            return usages;
        } catch (IOException e) {
            throw new RuntimeException("fail to parse usage CSV file: " + e.getMessage());
        }
    }

}
