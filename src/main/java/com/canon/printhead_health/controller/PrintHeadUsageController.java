package com.canon.printhead_health.controller;


import com.canon.printhead_health.business.UsageService;
import com.canon.printhead_health.domain.GetUsageDIUResponse;
import com.canon.printhead_health.domain.GetUsageResponse;
import com.canon.printhead_health.domain.ResponseData;
import com.canon.printhead_health.domain.ResponseMessage;
import com.canon.printhead_health.helper.CsvHelperPrintHeadUsage;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/printHeadUsage")
@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor

public class PrintHeadUsageController {

    @Autowired
    private UsageService printHeadUsageService;

    @GetMapping(value = "/id/{id}")
    public ResponseEntity<GetUsageResponse> getPrintHeadUsageById(
            @PathVariable Long id) {
        return ResponseEntity.ok(printHeadUsageService.getUsageById(id));
    }

    @GetMapping(value = "/date/{startDate}/{endDate}")
    public ResponseEntity<GetUsageResponse> getPrintHeadUsageByDate(
            @PathVariable LocalDate startDate, LocalDate endDate, @RequestParam(required = false) Long printer) {
        return ResponseEntity.ok(printHeadUsageService.getUsageByDate(startDate, endDate, printer));
    }


    @PostMapping(value = "/upload")
    public ResponseEntity<ResponseMessage> uploadPrintHeadUsage(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (CsvHelperPrintHeadUsage.hasCSVFormat(file)) {
            try {
                printHeadUsageService.save(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();
                return ResponseEntity.status(200).body(new ResponseMessage(message));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!" + e;
                return ResponseEntity.status(500).body(new ResponseMessage(message));
            }
        }

        message = "Please upload a csv file!";
        return ResponseEntity.status(400).body(new ResponseMessage(message));

    }

    @GetMapping(value = "/status")
    public Map<String, Boolean> status() {
        if(printHeadUsageService.isUpdated()) {
            return Collections.singletonMap("updated", true);
        } else {
            return Collections.singletonMap("updated", false);
        }
    }

    @GetMapping(value = "/getByParam/{paramString}/{startDate}/{endDate}/{printer}/{printHeadId}")
    public ResponseEntity<ResponseData> getPrintHeadUsageByDateAndMetricString(
            @PathVariable String paramString, @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate, @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate, @PathVariable Long printer, @PathVariable Long printHeadId) {
        return ResponseEntity.ok(printHeadUsageService.findByParamAndDate(paramString, startDate, endDate, printer, printHeadId));
    }


    @GetMapping(value = "/{startDate}/{endDate}/{printHeadId}/{printer}")
    public ResponseEntity<GetUsageDIUResponse> getDaysInUseByIdAndDate(
            @PathVariable("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @PathVariable("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate,
            @PathVariable("printHeadId") Long printHeadId,
            @PathVariable("printer") Long printer) {
        return ResponseEntity.ok(printHeadUsageService.getDaysInUse(startDate, endDate, printHeadId, printer));
    }
}