package com.canon.printhead_health.controller;

import com.canon.printhead_health.business.ErrorService;
import com.canon.printhead_health.domain.GetErrorCodeResponse;
import com.canon.printhead_health.domain.GetErrorResponse;
import com.canon.printhead_health.domain.ResponseMessage;
import com.canon.printhead_health.helper.CsvHelperError;
import com.canon.printhead_health.persistence.entity.ErrorEntity;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(value = "/printHeadError")
@CrossOrigin(origins = "*")
@AllArgsConstructor
public class PrintHeadErrorController {

    @Autowired
    private ErrorService printHeadErrorService;


    @GetMapping
    public ResponseEntity<GetErrorResponse> getAllErrors(@RequestParam(required = false) Long printer) {
        return ResponseEntity.ok(printHeadErrorService.getAllPrinterHeadErrors(printer));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<GetErrorResponse> getErrorByPrinterHeadId(@PathVariable Long id, @RequestParam(required = false) Long printer) {
        return ResponseEntity.ok(printHeadErrorService.getPrinterHeadErrorById(id, printer));
    }

    @GetMapping(value = "/{startDate}/{endDate}")
    public ResponseEntity<GetErrorResponse> filterErrorsByDate(@PathVariable("startDate")
                                                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                   LocalDate startDate,
                                                               @PathVariable("endDate")
                                                               @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                               LocalDate endDate,
                                                               @RequestParam(required = false) Long printer) {
        return ResponseEntity.ok(printHeadErrorService.filterErrorsByDate(startDate, endDate, printer));
    }

    @GetMapping(value = "/getTopErrors/{startDate}/{endDate}")
    public ResponseEntity<GetErrorCodeResponse> getTopErrors(@PathVariable("startDate")
                                                                 @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                         LocalDate startDate,
                                                             @PathVariable("endDate")
                                                                 @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
                                                                         LocalDate endDate,
                                                             @RequestParam(required = false) Long printer) {
        return ResponseEntity.ok(printHeadErrorService.getTop10Errors(startDate, endDate, printer));
    }

    @GetMapping(value = "/printer/{printer}")
    public ResponseEntity<GetErrorResponse> getErrorByPrinter(@PathVariable Long printer) {
        return ResponseEntity.ok(printHeadErrorService.getErrorByPrinter(printer));
    }


    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";

        if (CsvHelperError.hasCSVFormat(file)) {
            try {
                printHeadErrorService.save(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();

                //updated=true;

                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!" + e;
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }

        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }

    @GetMapping(value = "/update")
    public ResponseEntity<Boolean> update() {

        return null;
    }
    @GetMapping(value = "/status")
    public Map<String, Boolean> status() {
        if(printHeadErrorService.isUpdated()) {
            return Collections.singletonMap("updated", true);
        }
        else{
            return Collections.singletonMap("updated", false);

        }
    }
    @GetMapping(value = "/nrOfPrinters")
    public ResponseEntity<Long> getNumberOfPrinters() {
        return ResponseEntity.ok(printHeadErrorService.getNumberOfPrinters());
    }





}