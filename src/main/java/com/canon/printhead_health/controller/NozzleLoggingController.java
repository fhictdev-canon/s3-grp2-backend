package com.canon.printhead_health.controller;


import com.canon.printhead_health.business.NozzleLogService;
import com.canon.printhead_health.domain.GetNozzleLogResponse;
import com.canon.printhead_health.domain.ResponseDataNozzleLog;
import com.canon.printhead_health.domain.ResponseMessage;
import com.canon.printhead_health.helper.CsvHelperError;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;



import java.util.Collections;
import java.util.Map;
import java.time.LocalDateTime;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/nozzleLog")
@AllArgsConstructor
public class NozzleLoggingController {

    @Autowired
    private final NozzleLogService nozzleLogService;

    @GetMapping
    public ResponseEntity<GetNozzleLogResponse> getNozzles(@RequestParam(required = false) Long printer) {
        return ResponseEntity.ok(nozzleLogService.getNozzleLogs(printer));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<GetNozzleLogResponse> getNozzleLogById(@PathVariable Long id){
        return ResponseEntity.ok(nozzleLogService.getNozzleLogById(id));
    }

    @GetMapping(value = "/nozzle/{nozzle}")
    public ResponseEntity<GetNozzleLogResponse> getNozzleLogByNozzleNr(@PathVariable Long nozzle,@RequestParam(required = false) Long printer ){
        return ResponseEntity.ok(nozzleLogService.getNozzleByNozzle(nozzle, printer));
    }

    @GetMapping(value = "/date/{timestamp}")
    public ResponseEntity<GetNozzleLogResponse> getNozzleLogByDate(@PathVariable("timestamp")
                                                                       @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
                                                                       LocalDateTime timestamp,  @RequestParam(required = false) Long printer
    ){
        return ResponseEntity.ok(nozzleLogService.getNozzleLogsByDate(timestamp, printer));
    }


    @GetMapping(value = "/error/{error}")
    public ResponseEntity<GetNozzleLogResponse> getNozzleLogByError(@PathVariable String error, @RequestParam(required = false) Long printer){
        return ResponseEntity.ok(nozzleLogService.getNozzleLogByError(error, printer));
    }

    @GetMapping(value = "/chartData/{error}/{startDate}/{endDate}/{printer}")
    public ResponseEntity<ResponseDataNozzleLog> getNozzleLogByStartEndDateAndErrorAndPrinter(
            @PathVariable String error, @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate, @PathVariable @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate, @PathVariable Long printer) {
        return ResponseEntity.ok(nozzleLogService.findByErrorAndDateAndPrinter(error, startDate, endDate, printer));
    }

    @GetMapping(value = "/Filters")
    public ResponseEntity<GetNozzleLogResponse> getNozzleLogAllFilters(@RequestParam(required = false) String error, @RequestParam(required = false) Long nozzle,
                                                                       @RequestParam(required = false)
                                                                       @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime first,@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime second,
                                                                       @RequestParam(required = false) Long  printer){

        return ResponseEntity.ok(nozzleLogService.getNozzleLogByAllFilters(nozzle,error, first ,second, printer));
    }
    @GetMapping(value = "/Filters/Range")
    public ResponseEntity<GetNozzleLogResponse> getNozzleLogAllFilters(@RequestParam(required = false) String error, @RequestParam(required = false) Long nozzle,@RequestParam(required = false) Long nozzle2,
                                                                       @RequestParam(required = false)
                                                                       @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime first,@RequestParam(required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime second,
                                                                       @RequestParam(required = false) Long  printer){

        return ResponseEntity.ok(nozzleLogService.getNozzleLogByAllFilters(nozzle,nozzle2, error, first ,second, printer));
    }

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message ;

        if (CsvHelperError.hasCSVFormat(file)) {
            try {
                nozzleLogService.save(file);

                message = "Uploaded the file successfully: " + file.getOriginalFilename();

                //updated=true;

                return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
            } catch (Exception e) {
                message = "Could not upload the file: " + file.getOriginalFilename() + "!" + e;
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
            }
        }

        message = "Please upload a csv file!";
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(message));
    }

    @GetMapping(value = "/status")
    public Map<String, Boolean> status() {
        if (nozzleLogService.isUpdated()) {
            return Collections.singletonMap("updated", true);
        } else {
            return Collections.singletonMap("updated", false);

        }

    }


}
