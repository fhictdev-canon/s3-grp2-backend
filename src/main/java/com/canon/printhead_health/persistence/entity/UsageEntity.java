package com.canon.printhead_health.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "usage")
public class UsageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "usageId")
    private Long usageId;

    @Column(name = "printer")
    private Long printer;

    @Column(name = "usageDate")
    private LocalDate usageDate;

    @Column(name = "positionEngine")
    private Long positionInEngineId;

    @Column(name = "printHeadId")
    private Long printHeadId;

    @Column(name = "installTimeStamp")
    private LocalDate installTimeStamp;

    @Column(name = "lastUsedTimeStamp")
    private LocalDate lastUsedTimeStamp;

    @Column(name = "nofSquareMilliMetersPrinted")
    private Long nofSquareMmPrinted;

    @Column(name = "nofVibrationActuations")
    private Long nofVibrationActions;

    @Column(name = "nofJetActuations")
    private Long nofJetActuations;

    @Column(name = "nofColdStartups")
    private Long nofColdStartups;

    @Column(name = "inkUsageml")
    private Long inkUsageMl;

    @Column(name = "warmTimehr")
    private Long warmTimeHrs;

    @Column(name = "biasVoltageTimehr")
    private Long biasVoltageTimeHrs;

    @Column(name = "daysInUse")
    private Long daysInUse;
}
