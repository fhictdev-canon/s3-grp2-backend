package com.canon.printhead_health.persistence.entity;

import java.sql.Date;
import java.time.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "nozzlelogging")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NozzleLogEntity {
    @Id
    @NotNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "nozzleId")
    private Long nozzleLogId;

    @NotNull
    @Column(name = "printer")
    private Long printer;

    @NotNull
    @Column(name = "timestamp")
    private LocalDateTime timestamp;

    @NotNull
    @Column(name = "engineCycleId")
    private Long engineCycleId;

    @NotNull
    @Column(name = "swathId")
    private Long swathId;

    @NotNull
    @Column(name = "purpose")
    private String purpose;

    @NotNull
    @Column(name = "nozzle")
    private Long nozzle;

    @NotNull
    @Column(name = "error")
    private String error;

    @NotNull
    @Column(name = "chip")
    private Long chip;

}
