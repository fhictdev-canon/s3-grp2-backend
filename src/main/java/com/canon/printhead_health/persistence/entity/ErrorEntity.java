package com.canon.printhead_health.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.time.LocalDate;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "errors")
public class ErrorEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "errorId")
    private Long id;

    @Column(name = "printer")
    private Long printer;

    @Column(name = "timestamp")
    private LocalDate timestamp;

    @Column(name = "printheadId")
    private int printheadId;

    @Column(name = "errorEventNr")
    private int errorEventNr;

    @Column(name = "errorCode")
    private int errorCode;

}