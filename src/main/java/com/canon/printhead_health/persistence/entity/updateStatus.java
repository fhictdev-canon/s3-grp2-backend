package com.canon.printhead_health.persistence.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Entity
    @Table(name = "updateStatus")
    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public class updateStatus {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "Id")
        private Long id;

        @Column(name = "nozzleStatus",nullable = false)
        @Type(type = "org.hibernate.type.NumericBooleanType")
        private boolean nozzleStatus;

        @Column(name = "errorStatus",nullable = false)
        @Type(type = "org.hibernate.type.NumericBooleanType")
        private boolean errorStatus;

        @Column(name = "usageStatus",nullable = false)
        @Type(type = "org.hibernate.type.NumericBooleanType")
        private boolean usageStatus;

    }

