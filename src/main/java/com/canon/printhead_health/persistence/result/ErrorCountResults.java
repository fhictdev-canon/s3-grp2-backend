package com.canon.printhead_health.persistence.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

public class ErrorCountResults {
    private int errorCode;
    private Long errorCodesUsage;
    private Long printer;

    public ErrorCountResults(int errorCode, Long errorCodesUsage, Long printer){
        this.errorCode = errorCode;
        this.errorCodesUsage = errorCodesUsage;
        this.printer = printer;
    }
    public ErrorCountResults(int errorCode, Long errorCodesUsage){
        this.errorCode = errorCode;
        this.errorCodesUsage = errorCodesUsage;
    }

    public int getErrorCode(){
        return errorCode;
    }
    public Long getErrorCodesUsage(){
        return errorCodesUsage;
    }
    public Long getPrinter(){ return printer;}



}
