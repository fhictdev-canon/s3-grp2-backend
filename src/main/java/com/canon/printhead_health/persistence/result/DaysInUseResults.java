package com.canon.printhead_health.persistence.result;

public class DaysInUseResults {
    private Long daysInUse;

    public DaysInUseResults(Long daysInUse) {
        this.daysInUse = daysInUse;
    }

    public Long getDaysInUse() {
        return daysInUse;
    }
}
