package com.canon.printhead_health.persistence.repository;

import com.canon.printhead_health.persistence.entity.NozzleLogEntity;
import net.bytebuddy.asm.Advice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

public interface NozzleLogRepository extends JpaRepository <NozzleLogEntity,Long>{

    List<NozzleLogEntity> findAllByPrinter(Long printer);

    List<NozzleLogEntity> findAllByTimestampBetweenAndPrinter(
            LocalDateTime timestamp1,
            LocalDateTime timestamp2, Long printer);
    List<NozzleLogEntity> findAllByTimestampBetween(
            LocalDateTime timestamp1,
            LocalDateTime timestamp2);

    List<NozzleLogEntity> findAllByErrorAndPrinter(String error, Long printer);
    List<NozzleLogEntity> findAllByError(String error);

    List<NozzleLogEntity> findAllByNozzle(Long nozzle);
    List<NozzleLogEntity> findAllByNozzleAndPrinter(Long nozzle, Long printer);
    List<NozzleLogEntity> findAllByNozzleBetweenAndPrinter(Long nozzle,Long nozzle2, Long printer);


    List<NozzleLogEntity> findAllByTimestampBetweenAndNozzleAndErrorAndPrinter(LocalDateTime first,LocalDateTime second,Long nozzle,String error, Long printer);

    List<NozzleLogEntity> findAllByTimestampBetweenAndNozzleBetweenAndErrorAndPrinter(LocalDateTime first,LocalDateTime second,Long nozzle,Long nozzle2, String error, Long printer);


    List<NozzleLogEntity> findAllByNozzleAndErrorAndPrinter(Long nozzle,String error, Long printer);
    List<NozzleLogEntity> findAllByNozzleBetweenAndErrorAndPrinter(Long nozzle,Long nozzle2, String error, Long printer);


    List<NozzleLogEntity> findAllByTimestampBetweenAndNozzleAndPrinter(LocalDateTime first,LocalDateTime second,Long nozzle, Long printer);
    List<NozzleLogEntity> findAllByTimestampBetweenAndNozzleBetweenAndPrinter(LocalDateTime first,LocalDateTime second,Long nozzle,Long nozzle2, Long printer);


    List<NozzleLogEntity> findAllByTimestampBetweenAndErrorAndPrinter(LocalDateTime first,LocalDateTime second,String error, Long printer);








    List<NozzleLogEntity> findAllByTimestampBetweenAndPrinterAndError(LocalDateTime startDate, LocalDateTime endDate, Long printer, String error);

}
