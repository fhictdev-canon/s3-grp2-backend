package com.canon.printhead_health.persistence.repository;
import com.canon.printhead_health.persistence.entity.ErrorEntity;
import com.canon.printhead_health.persistence.entity.updateStatus;
import com.canon.printhead_health.persistence.result.ErrorCountResults;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

public interface updateStatusRepository extends JpaRepository<updateStatus, Long> {

    List<updateStatus>  findByErrorStatusTrue();

    List<updateStatus>  findByUsageStatusTrue();

    List<updateStatus>  findByNozzleStatusTrue();

    //UPDATE `canon-db`.`updateStatus` SET `errorStatus` = '1' WHERE (`Id` = '1');

    @Transactional
    @Modifying
    @Query("UPDATE updateStatus u set u.errorStatus = '1' where (u.id = 1)")
    void updateErrorStatusToTrue();

    @Transactional
    @Modifying
    @Query("UPDATE updateStatus u set u.errorStatus = '0' where (u.id = 1)")
    void updateErrorStatusToFalse();

    @Transactional
    @Modifying
    @Query("UPDATE updateStatus u set u.usageStatus = '1' where (u.id = 1)")
    void updateUsageStatusToTrue();

    @Transactional
    @Modifying
    @Query("UPDATE updateStatus u set u.usageStatus = '0' where (u.id = 1)")
    void updateUsageStatusToFalse();

    @Transactional
    @Modifying
    @Query("UPDATE updateStatus u set u.nozzleStatus = '1' where (u.id = 1)")
    void updateNozzleStatusToTrue();

    @Transactional
    @Modifying
    @Query("UPDATE updateStatus u set u.nozzleStatus = '0' where (u.id = 1)")
    void updateNozzleStatusToFalse();


}
