package com.canon.printhead_health.persistence.repository;

import com.canon.printhead_health.persistence.entity.ErrorEntity;
import com.canon.printhead_health.persistence.result.ErrorCountResults;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface PrinterHeadErrorRepository extends JpaRepository<ErrorEntity, Long> {



    @Query("select count(distinct e.printer) from ErrorEntity e")
    long countDistinctByPrinter();


    //ERROR BY PRINTER
    List<ErrorEntity> findAllByPrinter(Long printer);

    //ERROR BY ID
    List<ErrorEntity> findByIdAndPrinter(Long id, Long printer);

    //ERROR BY DATE
    List<ErrorEntity> findAllByTimestampBetweenAndPrinter(
            LocalDate timestamp1,
            LocalDate timestamp2, Long printer);
    List<ErrorEntity> findAllByTimestampBetween(
            LocalDate timestamp1,
            LocalDate timestamp2);

    //TOP 10 ERRORS
    @Query("SELECT new com.canon.printhead_health.persistence.result.ErrorCountResults(e.errorCode,COUNT(e.errorCode))" +
            "FROM ErrorEntity e \n" +
            "WHERE e.timestamp BETWEEN :firstDate AND :secondDate \n" +
            "GROUP BY e.errorCode \n" +
            "ORDER BY COUNT(e.errorCode) DESC \n")
    List<ErrorCountResults> countByErrorCode(@Param("firstDate") LocalDate firstDate,
                                             @Param("secondDate")LocalDate secondDate,
                                             PageRequest pageable);

    @Query("SELECT new com.canon.printhead_health.persistence.result.ErrorCountResults(e.errorCode,COUNT(e.errorCode), e.printer)" +
            "FROM ErrorEntity e \n" +
            "WHERE e.timestamp BETWEEN :firstDate AND :secondDate \n" +
            "AND e.printer = :printer \n" +
            "GROUP BY e.errorCode  \n" +
            "ORDER BY COUNT(e.errorCode) DESC \n")
    List<ErrorCountResults> countByErrorCodeAndPrinter(@Param("firstDate") LocalDate firstDate,
                                             @Param("secondDate")LocalDate secondDate, @Param("printer")Long printer,
                                             PageRequest pageable);





}
