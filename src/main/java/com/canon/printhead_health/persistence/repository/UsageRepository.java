package com.canon.printhead_health.persistence.repository;

import com.canon.printhead_health.persistence.entity.UsageEntity;
import com.canon.printhead_health.persistence.result.DaysInUseResults;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface UsageRepository extends JpaRepository <UsageEntity,Long>{

    List<UsageEntity> findAllByUsageDateBetween(LocalDate startDate, LocalDate endDate);
    List<UsageEntity> findAllByUsageDateBetweenAndPrinter(LocalDate startDate, LocalDate endDate, Long printer);

    List<UsageEntity> findAllByUsageDateBetweenAndPrinterAndPrintHeadId(LocalDate startDate, LocalDate endDate, Long printer, Long printHeadId);

    @Query("SELECT new com.canon.printhead_health.persistence.result.DaysInUseResults(u.daysInUse)" +
            "FROM UsageEntity u \n" +
            "WHERE u.usageDate BETWEEN :firstDate AND :secondDate \n" +
            "AND u.printHeadId = :printHeadId \n" +
            "AND u.printer = :printer \n" +
            "GROUP BY u.daysInUse \n" +
            "ORDER BY COUNT(u.daysInUse) ASC \n"
    )
    List<DaysInUseResults> findAllByUsageDateAndPrintHeadIdAndPrinter(@Param("firstDate") LocalDate firstDate,
                                                                      @Param("secondDate") LocalDate secondDate,
                                                                      @Param("printHeadId") Long printHeadId,
                                                                      @Param("printer") Long printer,
                                                                      PageRequest pageable);
}