package com.canon.printhead_health;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrintHeadHealthApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrintHeadHealthApplication.class, args);
    }

}
