package com.canon.printhead_health.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorCodeList {
    private int codeError;
    private Long codeCodesUsage;
    private Long printer;
}
