package com.canon.printhead_health.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Error {

    private Long id;
    private Long printer;
    private LocalDate timestamp;
    private int printheadId;
    private int errorEventNr;
    private int errorCode;

}
