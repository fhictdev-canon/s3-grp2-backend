package com.canon.printhead_health.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class ResponseDataNozzleLog {
    private List<NozzleLogXYData> nozzleLogXYData;
}
