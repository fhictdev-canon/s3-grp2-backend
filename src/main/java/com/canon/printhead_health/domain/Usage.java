package com.canon.printhead_health.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Usage {
    private Long usageId;
    private Long printer;
    private LocalDate usageDate;
    private Long positionInEngineId;
    private Long printHeadId;
    private LocalDate installTimeStamp;
    private LocalDate lastUsedTimeStamp;
    private Long nofSquareMmPrinted;
    private Long nofVibrationActions;
    private Long nofJetActuations;
    private Long nofColdStartups;
    private Long inkUsageMl;
    private Long warmTimeHrs;
    private Long biasVoltageTimeHrs;
    private Long daysInUse;
}
