package com.canon.printhead_health.domain;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class ResponseData {
    private List<Float> x;
    private List<LocalDate> y;
}
