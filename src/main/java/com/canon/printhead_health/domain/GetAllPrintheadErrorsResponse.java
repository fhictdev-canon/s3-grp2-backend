package com.canon.printhead_health.domain;

import com.canon.printhead_health.persistence.entity.ErrorEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetAllPrintheadErrorsResponse {
    private List<ErrorEntity> errors;
}
