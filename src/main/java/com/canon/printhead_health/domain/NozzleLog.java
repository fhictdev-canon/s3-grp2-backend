package com.canon.printhead_health.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NozzleLog {
    private Long nozzleLogId;
    private Long printer;
    private LocalDateTime timestamp;
    private Long engineCycleId;
    private Long swathId;
    private String purpose;
    private Long nozzle;
    private String error;
    private Long chip;
}
