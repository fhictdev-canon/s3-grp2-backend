package com.canon.printhead_health.domain;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class GetUsageDIUResponse {
    private List<UsageDIU> usageDIU;
}
